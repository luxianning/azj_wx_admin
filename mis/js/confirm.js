function art_del_confirm(url, message) {
	art.dialog({
		title : '确认删除',
		okValue : '确认',
		cancelValue : '取消',
		width : 230,
		height : 100,
		fixed : true,
		content : message,
		ok : function() {
			window.location.href = url;
			return true;
		},
		cancel : function() {
			return true;
		},
	});
}
function art_notify_confirm(url, message) {
	art.dialog({
		title : '提示',
		okValue : '确认',
		cancelValue : '取消',
		width : 230,
		height : 100,
		fixed : true,
		content : message,
		ok : function() {
			window.location.href = url;
			return true;
		},
		cancel : function() {
			return true;
		},
	});
}

function art_ajax_confirm(url, message, id) {
	art.dialog({
		title : '提示',
		okValue : '确认',
		cancelValue : '取消',
		width : 230,
		height : 100,
		fixed : true,
		content : message,
		ok : function() {
		$.post(url, { id: id},
			function(data){
			if(data.status != 0)
				alert(data.message);
			else
				alert('操作成功');
			},"json");
			return true;
		},
		cancel : function() {
			return true;
		},
	});
}
function art_remark_confirm(url, message) {
	art.dialog({
		title : '提示',
		okValue : '确认',
		cancelValue : '取消',
		width : 230,
		height : 100,
		fixed : true,
		content : message,
		ok : function() {
		$.post(url, {remark:$("#remark").val()},
			function(data){
			if(data.status != 0)
				alert(data.message);
			else
			{
				alert('操作成功');
				window.location.reload();
			}
				
			},"json");
			return true;
		},
		cancel : function() {
			return true;
		},
	});
}
function art_help_times_confirm(url, message) {
	art.dialog({
		title : '提示',
		okValue : '确认',
		cancelValue : '取消',
		width : 230,
		height : 100,
		fixed : true,
		content : message,
		ok : function() {
		$.post(url, {help_times:$("#help_times").val()},
			function(data){
			if(data.status != 0)
				alert(data.message);
			else
			{
				alert('操作成功');
				window.location.reload();
			}
				
			},"json");
			return true;
		},
		cancel : function() {
			return true;
		},
	});
}

function art_img(title,url,qr_url)
{
art.dialog({padding: 0,title: title,content: '<div style="width:380px">活动链接: '+url+'</div> <img src="'+qr_url+'" width="400" height="400" />',lock: true});
}

