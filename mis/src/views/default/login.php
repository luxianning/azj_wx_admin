<?php
$no_visible_elements = true;
include('header.php'); ?>

    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>爱自驾管理系统登录</h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
		请输入用户名密码
            </div>

<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'loginf',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
                'validateOnSubmit'=>true,
        ),
)); ?>

                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <input id="username" type="text" class="form-control" placeholder="Username" name="LoginForm[username]">
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input id="password" type="password" class="form-control" placeholder="Password" name="LoginForm[password]">
                    </div>
                    <div class="clearfix"></div>

<!--
                    <div class="input-prepend">
                        <label class="remember" for="remember"><input type="checkbox" id="remember" name="LoginForm[rememberMe]"> Remember me</label>
                    </div>
                    <div class="clearfix"></div>
-->

                    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary"> 登 录 </button>
                    </p>
                </fieldset>
<?php $this->endWidget(); ?>
        </div>
        <!--/span-->
    </div><!--/row-->

<script type="text/javascript">

if(parent.document.getElementById('leftmenu'))
{
	window.parent.location.replace(document.location);
}

dz_login_init = false;

function dz_login(data)
{
	url = '../w/api/uc.php?time='+data['time']+'&code='+data['code'];
	$.get(url, function(data){
		dz_login_init = true;
		$('#loginf').submit();
	});
}

$('#loginf').submit(function() {

	if(dz_login_init)
	{
		return true;
	}

	$.ajax({
		type: "POST",
		url: "index.php?r=default/ucode",
		data: {username:$("#username").val(), password:$("#password").val()},
		dataType: "json",
		success: function(data){
			dz_login(data);
		}
	});


	// 为了防止普通浏览器进行表单提交和产生页面导航（防止页面刷新？）返回false
	return false;
});

</script>

<?php require('footer.php'); ?>
