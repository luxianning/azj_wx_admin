<div>    
	<ul class="breadcrumb">
		<li><a href="./index.php">首页</a></li>
		<li>渠道管理</li>
	</ul>
</div>
<div class="control-group form-inline">
<label>选择时间：</label> <input id='start_time'  name='start_time'
value="<?php echo $date; ?>"
class="form-control">
</div>
<script>
$( "#start_time" ).datepicker({ 
	dateFormat: "yy-mm-dd" ,
      	onClose: function( selectedDate ) {
		window.location.href="?r=channel/admin&date="+$("#start_time").val();
      	}
});
</script>
<?php $this->renderPartial('channel_list',array(
			'dataProvider'=>$dataProvider,
			)); ?>

