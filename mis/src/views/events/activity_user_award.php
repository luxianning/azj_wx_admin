<div>
	<ul class="breadcrumb">
		<li><a href="./index.php">首页</a></li>
		<li><a href="./index.php?r=events/admin">活动管理</a></li>
		<li>获奖名单情况</li>
	</ul>
</div>  
<?php 
if(empty($dataProvider)){
	echo "<h3>该活动无获奖名单</h3>";
}else{?>
	<h3>活动 <?php echo $title;?> 获奖情况</h3>
	<a class="btn btn-success" href="./index.php?r=events/Award&id=<?php echo $id;?>&export=1"> <i
			class="glyphicon glyphicon-trash icon-white"></i> 导出
	</a>
<!-- tab_start -->
<?php 
	$this->renderPartial('activity_user_award_list',array(
	'dataProvider'=>$dataProvider,
));
}
?>
<!-- tab_end -->

