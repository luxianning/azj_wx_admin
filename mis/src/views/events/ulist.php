<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well" data-original-title="活动列表">
				<h2>
					<i class="glyphicon glyphicon-user"></i> 活动列表
				</h2>

				<div class="box-icon">
					<a href="#" class="btn btn-minimize btn-round btn-default"><i
						class="glyphicon glyphicon-chevron-up"></i></a> <a href="#"
						class="btn btn-close btn-round btn-default"><i
						class="glyphicon glyphicon-remove"></i></a>
				</div>
			</div>
			<div class="box-content">
				<table class="table table-striped table-bordered responsive">
					<thead>
						<tr>
							<th>姓名</th>
							<th>手机</th>
							<th>EMail</th>
							<th>性别</th>
							<th>年龄</th>
							<th>学历</th>
							<th>爱好</th>
							<th>签到次数</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach ( $list as $v ) {
					if($v['sex'] == 'F'){
						$v['sex'] = '女';
					}elseif($v['sex'] == 'M'){
						$v['sex'] = '男';
					}
						?>
						<tr>
							<td><?php echo $v['name']?></td>
							<td class="center"><?php echo $v['mtel']?></td>
							<td class="center"><?php echo $v['email']?></td>
							<td class="center"><?php echo $v['sex']?></td>
							<td class="center"><?php echo $v['age']?></td>
							<td class="center"><?php echo $v['edu']?></td>
							<td class="center"><?php echo $v['habby']?></td>
							<td class="center"><?php echo $v['count']?></td>
							<td class="center"><a class="btn btn-danger"
								href="./index.php?r=events/Deluser&id=<?php echo $v['id']?>"> <i
									class="glyphicon glyphicon-trash icon-white"></i> Delete
							</a></td>
						</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
