<div>
	<ul class="breadcrumb">
		<li><a href="./index.php">首页</a></li>
		<li><a href="./index.php?r=events/admin">活动报名</a></li>
		<li>活动报名情况</li>
	</ul>
</div>  
<?php 
if(empty($dataProvider)){
	echo "<h3>该活动无人报名</h3>";
}else{?>
	<h3>活动 <?php echo $title;?> 报名情况</h3>
	<a class="btn btn-success" href="./index.php?r=events/Apply&id=<?php echo $id;?>&export=1"> <i
			class="glyphicon glyphicon-trash icon-white"></i> 导出
	</a>
<!-- tab_start -->
<?php 
	$this->renderPartial('apply_list',array(
	'dataProvider'=>$dataProvider,
));
}
?>
<!-- tab_end -->

