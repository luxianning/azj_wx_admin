<div class="gdtjContainer">
<table  border="1" style="text-align: left;">
<thead>
<tr>
<th >排名</th>
<th >奖品类型</th>
<th >助力数</th>
<th >关注数</th>
<th >获奖者昵称</th>
<th >姓名</th>
<th >性别</th>
<th >身份照号</th>
<th >地址</th>
<th >手机号</th>
</tr>
</thead>
<tbody>
<?php
if(!empty($data)){
    foreach( $data as $v ){
?>
<tr>
	<td nowrap="nowrap"><?php echo $v->user_id;  ?></td>
	<td nowrap="nowrap"><?php echo $v->award->title; ?></td>
        <td nowrap="nowrap"><?php echo $v->activity_user->help_times; ?></td>
        <td nowrap="nowrap"><?php echo $v->activity_user->subscribe; ?></td>
        <td nowrap="nowrap"><?php echo $v->user->nickname?$v->user->nickname:''; ?></td>
        <td nowrap="nowrap"><?php echo $v->realname?$v->realname:''; ?></td>
        <td nowrap="nowrap"><?php echo $v->user->sex == 1 ? '男' : ($v->user->sex == 2 ? '女':'未知'); ?></td>
        <td nowrap="nowrap"><?php echo $v->idno?$v->idno:''; ?></td>
        <td nowrap="nowrap"><?php echo $v->address?$v->address:''; ?></td>
        <td nowrap="nowrap"><?php echo $v->mobile?$v->mobile:''; ?></td>
</tr>    
</tr>
<?php }}?>
</tbody>
</table>
</div>
