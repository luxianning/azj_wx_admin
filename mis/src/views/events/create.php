<style>.box-content label{max-width:550px;min-width:150px}</style>
<div> 
	<ul class="breadcrumb">
		<li><a href="./index.php">首页</a></li>
		<li><a href="./index.php?r=events/admin">活动管理</a></li>
		<li>创建活动</li>
	</ul>
</div>
<div class="row">
	<div class="box col-md-12">
		<div class="box-inner">
			<div class="box-header well" data-original-title="">
				<h2>
					<i class="glyphicon glyphicon-edit"></i> 创建活动
				</h2>
				<!--  
				<div class="box-icon">
					<a href="#" class="btn btn-setting btn-round btn-default"><i
						class="glyphicon glyphicon-cog"></i></a> <a href="#"
						class="btn btn-minimize btn-round btn-default"><i
						class="glyphicon glyphicon-chevron-up"></i></a> <a href="#"
						class="btn btn-close btn-round btn-default"><i
						class="glyphicon glyphicon-remove"></i></a>
				</div>
				-->
			</div>
			<form id="mainform" action="./index.php?r=events/create"
				method="post">
				<div class="box-content">
					<div class="control-group form-inline">
						<label>活动名称：</label> <input id="title" name='title' class="form-control" style="width:500px"
							placeholder="请输入活动标题"
							value="<?php if(isset($content['title'])) echo $content['title'];?>">
						<span><span style="color:red">*</span>30字以内</span>
					</div><br>

					<div class="control-group form-inline">
						<label>活动关键词：</label> <input id="keyword" type='text' name='keyword'
							class="form-control"
							value="<?php if(isset($content['keyword'])) echo $content['keyword'];?>">
						<span><span style="color:red">*</span>5字以内，关键字+关注数，  就是查看活动邀请好友数。</span>
					</div><br>
					<div class="control-group form-inline">
						<label>图文封面(建议大小76KB，建议尺寸750x430，高度不限制)：</label>
						<div id='showthumb'>
						<?php if(isset($content['thumb'])){?>
							<img width='150' src='<?php echo 'http://' . Yii::app()->params->upload_host.$content['thumb'];?>'>
						<?php }?>   
						</div>
						<input name='upfile' id='fileToUploadthumb' type="file" class='form-control' style="width:10%" onchange="ajaxFileUpload('thumb')">
						<input type='text' class='form-control' style="width:60%"
							id='thumb' name='thumb' 
							value="<?php if(isset($content['thumb'])) echo 'http://' . Yii::app()->params->upload_host.$content['thumb'];?>">
					</div><br>

					<div class="control-group">
						<label>图文介绍：</label>
						<textarea class="form-control" id="summary" name='summary' cols="30" rows="4"><?php if(isset($content['summary'])) echo $content['summary'];?></textarea>
						<span><span style="color:red">*</span>100字以内</span>
					</div><br> 

					<div class="control-group form-inline">
						<label>分享logo(建议大小23KB，建议尺寸200x200)：</label>
						<div id='showshare_logo'>
						<?php if(isset($content['share_logo'])){?>
							<img width='150' src='<?php echo 'http://' . Yii::app()->params->upload_host.$content['share_logo'];?>'>
						<?php }?>   
						</div>
						<input name='upfile' id='fileToUploadshare_logo' type="file" class='form-control' style="width:10%" onchange="ajaxFileUpload('share_logo')">
						<input type='text' class='form-control' style="width:60%"
							id='share_logo' name='share_logo' 
							value="<?php if(isset($content['share_logo'])) echo 'http://' . Yii::app()->params->upload_host.$content['share_logo'];?>">
					</div><br>

					<div class="control-group form-inline">
						<label>分享标题：</label> <input id="short_title" name='short_title' class="form-control" style="width:500px"
							placeholder="请输入分享标题"
							value="<?php if(isset($content['short_title'])) echo $content['short_title'];?>">
						<span><span style="color:red">*</span>25字以内</span>
					</div><br>

					<div class="control-group form-inline">
						<label>分享简介：</label> <input id="share_desc" name='share_desc' class="form-control" style="width:500px"
							placeholder="请输入分享简介"
							value="<?php if(isset($content['share_desc'])) echo $content['share_desc'];?>">
						<span><span style="color:red">*</span>36字以内</span>
					</div><br>
					<div class="control-group form-inline">
						<label>活动起止时间：</label> <input id='start_time'  name='start_time'
							value="<?php if(isset($content['start_time'])){echo $content['start_time'];}else{echo date('Y-m-d' , time());} ?>"
							class="form-control">
						<input id='end_time' name='end_time'
							value="<?php if(isset($content['end_time'])){echo $content['end_time'];}else{echo date('Y-m-d' , time());} ?>"
							class="form-control">
					</div><br>
                    <!--
					<div class="control-group form-inline">
						<label>报名起止时间</label> <input id='apply_start_time'  name='apply_start_time'
							value="<?php if(isset($content['apply_start_time'])){echo $content['apply_start_time'];}else{echo date('Y-m-d' , time());} ?>"
							class="form-control">
						<input id='apply_end_time' name='apply_end_time'
							value="<?php if(isset($content['apply_end_time'])){echo $content['apply_end_time'];}else{echo date('Y-m-d' , time());} ?>"
							class="form-control">
					</div><br>
                    --!>
					<div class="control-group">
						<label>活动内容：</label>
						<textarea class="form-control" id="content" name='content' cols="30" rows="4"><?php if(isset($content['content'])) echo $content['content'];?></textarea>
						<span><span style="color:red">*</span>1000字以内</span>
					</div><br>
			
					<div class="control-group form-inline">
						<label>活动大头图(建议大小320KB，建议尺寸750x1134，高度不限制)：</label>
						<div id='showimg'>
						<?php if(isset($content['img'])){?>
							<img width='150' src='<?php echo 'http://' . Yii::app()->params->upload_host.$content['img'];?>'>
						<?php }?>   
						</div>
						<input name='upfile' id='fileToUploadimg' type="file" class='form-control' style="width:10%" onchange="ajaxFileUpload('img')">
						<input type='text' class='form-control' style="width:60%"
							id='img' name='img' 
							value="<?php if(isset($content['img'])) echo 'http://' . Yii::app()->params->upload_host.$content['img'];?>">

					</div><br>

					<div class="control-group form-inline">
						<label>邀请卡背景图(建议大小360KB，图片尺寸750x1334，头像纵坐标60px，昵称纵坐标200px，二维码纵坐标600px，头像尺寸120x120，二维码尺寸330x330)：</label>
						<div id='showinvite_img'>
						<?php if(isset($content['invite_img'])){?>
							<img width='150' src='<?php echo 'http://' . Yii::app()->params->upload_host.$content['invite_img'];?>'>
						<?php }?>   
						</div>
						<input name='upfile' id='fileToUploadinvite_img' type="file" class='form-control' style="width:10%" onchange="ajaxFileUpload('invite_img')">
						<input type='text' class='form-control' style="width:60%"
							id='invite_img' name='invite_img' 
							value="<?php if(isset($content['invite_img'])) echo 'http://' . Yii::app()->params->upload_host.$content['invite_img'];?>">

					</div><br>

                    <div id="award_list" style="margin-left:30px">
                    <?php foreach($award as $key=>$row){ ?>
                    <div id="award<?php echo $key;?>" class="award">
					<div class="control-group form-inline">
                    <h3><?php echo $row['title']?></h3>
					</div>
					<div class="control-group form-inline">
						<label>奖品类别</label> <?php echo $row['title'];?><input type='hidden' name='award_title[]'
							class="form-control"
							value="<?php echo $row['title'];?>">
							&nbsp;&nbsp;<button class="del" value="<?php echo $row['title'];?>" order="<?php echo $key;?>">删除</button>
					</div><br>
					<div class="control-group form-inline">
						<label>奖品名称</label> <input type='text' name='award_name[]'
							class="form-control"
							value="<?php echo $row['name'];?>">
						<span><span style="color:red">*</span>10字以内</span>
					</div><br>
					<div class="control-group form-inline">
						<label>奖品描述</label> <input type='text' name='award_describe[]'
							class="form-control"
							value="<?php echo $row['describe'];?>">
						<span><span style="color:red">*</span>30字以内</span>
					</div><br>
                    <?php if($row['type'] == 1){ ?>
					<div class="control-group form-inline">
						<label>奖品规则</label> 关注数大于<input type='text' name='award_subscribe_num'
							class="form-control"
							value="<?php echo $row['subscribe_num'];?>">
						<span><span style="color:red">*</span>只能填写正整数</span>
					</div><br>
                    <?php } ?>
					<div class="control-group form-inline">
						<label>奖品数量</label> <input type='text' name='award_total[]'
							class="form-control"
							value="<?php echo $row['total'];?>">
						<span><span style="color:red">*</span>只能填写正整数</span>
					</div><br>
					<div class="control-group form-inline">
						<label>奖品图片(220x166)</label>
						<div id='showaward_img<?php print $key?>'>
						<?php if($row['img']){?>
							<img width='150' src='<?php echo 'http://' . Yii::app()->params->upload_host.$row['img'];?>'>
						<?php }?>   
						</div>
						<input name='upfile' id='fileToUploadaward_img<?php print $key?>' type="file" class='form-control' style="width:10%" onchange="ajaxFileUpload('award_img<?php print $key?>')">
						<input type='text' class='form-control' style="width:60%"
							id='award_img<?php print $key?>' name='award_img[]' 
							value="<?php echo 'http://' . Yii::app()->params->upload_host.$row['img'];?>">

					</div><br>
                    </div>
                    <?php }?>
                    </div>
					<div class="control-group form-inline">
                        <label>选择奖品类别：</label> <select id='award_type'>
                    <?php foreach($award_opt as $row){ ?>
                            <option value="<?php print $row;?>"><?php print $row;?></option>
			<?php }?>
                            </select>
                            <input type="button" id="add_award" value="添加奖品">
					</div><br>
					<div class="control-group">
						<label>领奖设置 ： </label>  
							<?php
							if (isset ( $content ['ID_card_status'] ) && $content ['ID_card_status'] == 1) {
								echo '<label class="radio-inline"><input type="radio" value="0" name="ID_card_status"> 不需要身份证信息</label>';
								echo '<label class="radio-inline"> <input type="radio" value="1" name="ID_card_status" checked> 需要身份证信息</label>';
							}
							else {
								echo '<label class="radio-inline"><input type="radio" value="0" name="ID_card_status" checked> 不需要身份证信息</label>';
								echo '<label class="radio-inline"> <input type="radio" value="1" name="ID_card_status" > 需要身份证信息</label>';
							}
							?>
                    </div><br>
				<div class="control-group form-inline">
					<input type="hidden" name="id"
						value="<?php echo isset($content['id']) ? $content['id'] : 0?>"> <input
						type="hidden" name="op"
						value="<?php echo isset($content['title']) ? 'update' : 'create'?>">
					<input class="submit btn btn-primary" type="submit"
						value='<?php echo isset($content['title']) ? '保存活动' : '创建活动'?>'>
				</div>
			</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">

	function ajaxFileUpload(id){
		$.ajaxFileUpload
		(
		 {
			url:'./js/ue/php/controller.php?action=uploadimage', //你处理上传文件的服务端
			secureuri:false,
			fileElementId:'fileToUpload'+id,
			dataType: 'json',
			success: function (data)
			{
				if( data.state = 'SUCCESS'){
					$("#show"+id).html("<img width='150' src="+ data.url+" />");
					$("#"+id).attr("value",data.url);
					$('#'+id).focus(); 
				}
				//alert(data.state);
			}
		}
		)
	}
// 	$('#fileToUpload').change(function(){
// 		var fileToUpload = $('#fileToUpload').val();
// 		if(fileToUpload == '' ){
// 			alert('请选择上传图片');
// 			return false;
// 		}

// 		$.ajaxFileUpload
// 			(
// 			 {
// 				url:'./js/ue/php/controller.php?action=uploadimage', //你处理上传文件的服务端
// 				secureuri:false,
// 				fileElementId:'fileToUpload',
// 				dataType: 'json',
// 				success: function (data)
// 				{
// 					if( data.state = 'SUCCESS'){
// 						$("#showimg").html("<img width='150' src="+ data.url+" />");
// 						$("#img1").attr("value","http://"+document.domain+data.url);
// 						$('#img1').focus(); 
// 					}
// 					//alert(data.state);
// 				}
// 			}
// 			)
// 		return true;
// 	});


	$(function(){
		$('#mainform').submit(function(){
			if($("#title").val().length > 30 || $("#title").val().length == 0)
			{
				alert('标题长度不符合规范');
				return false;
			}
			if($("#keyword").val().length > 5 || $("#keyword").val().length == 0)
			{
				alert('关键词长度不符合规范');
				return false;
			}
			if($("#summary").val().length > 100|| $("#summary").val().length == 0)
			{
				alert('图文介绍长度不符合规范');
				return false;
			}
			if($("#short_title").val().length > 25|| $("#short_title").val().length == 0)
			{
				alert('分享标题长度不符合规范');
				return false;
			}
			if($("#share_desc").val().length > 36 || $("#share_desc").val().length == 0)
			{
				alert('分享简介词长度不符合规范');
				return false;
			}

			var start_time = new Date(($('#start_time').val()).replace(/-/g,"/")).getTime();
			var end_time = new Date(($('#end_time').val()).replace(/-/g,"/")).getTime();
			if(start_time > end_time){
				alert('开始时间不能大于结束时间')
				$('#start_time').focus();
				return false;
			}
			return true;
			})
	});
</script>
<script type="text/javascript">
//var um = UM.getEditor('editor');
<?php
/*
if (isset ( $content ['content'] )) {
	echo "um.ready(function() {	um.setContent(". json_encode($content ['content']) . ");});";
}*/
?>
</script>
<script type="text/javascript">
$(function() {

$( "#start_time" ).datepicker({
	dateFormat: "yy-mm-dd",
      defaultDate: "+1w",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#end_time" ).datepicker( "option", "minDate", selectedDate );
      }
    });
    $( "#end_time" ).datepicker({
dateFormat: "yy-mm-dd",
      defaultDate: "+1w",
      changeMonth: true,
      onClose: function( selectedDate ) {
        $( "#start_time" ).datepicker( "option", "maxDate", selectedDate );
      }
    });

$( "#apply_start_time" ).datepicker({ dateFormat: "yy-mm-dd" });
$( "#apply_end_time" ).datepicker({ dateFormat: "yy-mm-dd" });



$("#award_list").delegate(".del","click",function(){ 
	$("#award"+$(this).attr("order")).remove();
	var value = $(this).attr("value"); 
	$("#award_type").append("<option value='"+value+"'>"+value+"</option>");
}); 


$( "#add_award" ).click(function()
{
    var award_type = $("#award_type").val();
$("#award_type option[value='"+award_type+"']").remove(); 
var len = $(".award").length+1;
	var p1 = [
		'<div id="award'+len+'" class="award">',
	'<div class="control-group form-inline">',
	'<label>奖品类别：</label> <input type="hidden" name="award_title[]" class="form-control" value="'+award_type+'">'+award_type,
	'&nbsp;&nbsp;<button class="del" value="'+award_type+'" order='+len+'>删除</button></div><br>',
	'<div class="control-group form-inline">',
	'<label>奖品名称：</label><input type="text" name="award_name[]" class="form-control" value="">',
	'<span><span style="color:red">*</span>10字以内</span>',
	'</div><br>',
	'<div class="control-group form-inline">',
	'<label>奖品描述：</label><input type="text" name="award_describe[]" class="form-control" value="">',
	'<span><span style="color:red">*</span>30字以内</span>',
	'</div><br>',
	];
	var p2='';
	if(award_type == '纪念奖')
		p2 ='<div class="control-group form-inline"><label>奖品规则</label> 关注数大于<input type="text" name="award_subscribe_num" class="form-control" value=""><span><span style="color:red">*</span>只能填写正整数</span></div><br>';
	var p3 = [
	'<div class="control-group form-inline">',
	'<label>奖品数量：</label><input type="text" name="award_total[]" class="form-control" value="">',
	'<span><span style="color:red">*</span></span>',
	'</div><br>',
	'<div class="control-group form-inline">',
	'<label>奖品图片(220x166)：</label>',
	'<div id="showaward_img'+len+'">',
	'</div>',
	'<input name="upfile" id="fileToUploadaward_img'+len+'" type="file" class="form-control" style="width:10%" onchange="ajaxFileUpload(\'award_img'+len+'\')">',
	'<input type="text" id="award_img'+len+'" name="award_img[]" class="form-control" value="">',
	'</div><br>',
	'</div>',
	];
	
    $( "#award_list" ).append(p1.join("")+p2+p3.join(""));
});
});

$('.submit').click(function(){
		if(ok1 && ok2 && ok3 && ok4){
		$('form').submit();
		}else{

		return false;
		}
		});
</script>
