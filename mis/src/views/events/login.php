<?php
$no_visible_elements = true;
include('header.php'); ?>

    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>爱自驾管理系统登录</h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
		请输入用户名密码
            </div>

<?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'login-form',
        'enableClientValidation'=>true,
        'clientOptions'=>array(
                'validateOnSubmit'=>true,
        ),
)); ?>

                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <input type="text" class="form-control" placeholder="Username" name="LoginForm[username]">
                    </div>
                    <div class="clearfix"></div><br>

                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock red"></i></span>
                        <input type="password" class="form-control" placeholder="Password" name="LoginForm[password]">
                    </div>
                    <div class="clearfix"></div>

<!--
                    <div class="input-prepend">
                        <label class="remember" for="remember"><input type="checkbox" id="remember" name="LoginForm[rememberMe]"> Remember me</label>
                    </div>
                    <div class="clearfix"></div>
-->

                    <p class="center col-md-5">
                        <button type="submit" class="btn btn-primary"> 登 录 </button>
                    </p>
                </fieldset>
<?php $this->endWidget(); ?>
        </div>
        <!--/span-->
    </div><!--/row-->
<?php require('footer.php'); ?>
