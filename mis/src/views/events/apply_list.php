<?php
$this->widget('ext.widgets.CTableView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'apply_item',
	'itemHeaderView'=>'apply_head',
	'itemsTagName'=>'table',
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'summaryText'=>'共{count}条，当前页显示第{start}-{end}条',
	'sortableAttributes'=>array('help_times','subscribe'),
	'sorterHeader' => '排序方式:',
	'template' => '{summary}{sorter}{items}{pager}', 
// 	'attributes'=>array( 'id', 'openid', 'name', 'mtel', 'email', 'sex', 'age', 'edu', 'habby' ),
	'pager'=>array(
		'class'=>'CLinkPager',//定义要调用的分页器类，默认是CLinkPager，需要完全自定义，还可以重写一个，参考我的另一篇博文：http://blog.sina.com.cn/s/blog_71d4414d0100yu6k.html
		'cssFile'=>false,//定义分页器的要调用的css文件，false为不调用，不调用则需要亲自己css文件里写这些样式
		'header'=>'',//定义的文字将显示在pager的最前面
		'footer'=>'',//定义的文字将显示在pager的最后面
		'firstPageLabel'=>'首页',//定义首页按钮的显示文字
		'lastPageLabel'=>'尾页',//定义末页按钮的显示文字
		'nextPageLabel'=>'下一页',//定义下一页按钮的显示文字
		'prevPageLabel'=>'前一页',//定义上一页按钮的显示文字
		//关于分页器这个array，具体还有很多属性，可参考CLinkPager的API
		),
	)
);
$this->beginWidget('zii.widgets.jui.CJuiDialog', array(
    'id'=>'mydialog',
    // additional javascript options for the dialog plugin
    'options'=>array(
        'title'=>Yii::t('query','修改助力数'),
        'autoOpen'=>false,
        'modal'=>'true',
        'width'=>'300',
	'height'=>'300',
	'buttons'=>array(
		'确定'=>'js:function(){ $.get("?r=events/changehelp", {id:$(this).attr("au_id"), help_times:$("#help_times").val()} ); $(this).dialog("close");location.reload();}',
		),
	),
));

 $this->renderPartial('_edit_help_form');  
$this->endWidget('zii.widgets.jui.CJuiDialog');
