<div>
	<ul class="breadcrumb">
		<li><a href="./index.php">首页</a></li>
		<li><a href="./index.php?r=events/admin">活动报名</a></li>
		<li>活动榜单情况</li>
	</ul>
</div>  
<?php 
if(empty($dataProvider->getData())){
	echo "<h3>该活动无人报名</h3>";
}else{?>
	<h3>活动 <?php echo $title;?> 榜单情况</h3>
	<a class="btn btn-success" href="./index.php?r=events/sign&id=<?php echo $id;?>&export=1"> <i
			class="glyphicon glyphicon-trash icon-white"></i> 导出
	</a>
<?php if($send_award_status == 1) { echo "已发奖"; ?>
<?php } else if($end_time < date("Y-m-d H:i:s")){?>	

<?php echo CHtml::link(Yii::t('cmp','确认发奖'),'javascript:',array('class'=>'btn btn-success icon-white','onclick'=>'return art_notify_confirm("./index.php?r=events/confirmAward&id='.$id.'","确定要发奖？")'))?>
<?php } else echo "活动结束后才能发奖";?>
<!-- tab_start -->
<?php 
	$this->renderPartial('apply_list',array(
	'title'=>$title,
	'dataProvider'=>$dataProvider,
));
}
?>
<!-- tab_end -->

