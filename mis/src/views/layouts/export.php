<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office"
xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40" >

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="en" />
    <style type="text/css">
        .td
        {
            width: 84px;
        }
        .gdtjContainer .tb tr
        {
            text-align: center;
            vertical-align: middle;
        }
        .gdtjContainer .tb th
        {
            border-left: 0.5pt solid #000;
            border-bottom: 0.5pt solid #000;
            text-align: center;
            font-weight: normal;
            font-size: 10pt;
            middle: ;;height:30px;}
        .gdtjContainer .header th
        {
            font-size: 12pt;
        }
        .gdtjContainer .tb tr th.noleftborder
        {
            border-left: none;
        }
        .gdtjContainer .tb tr th.rightborder
        {
            border-right: 0.5pt solid #000;
        }
    </style>
    
</head>

<body>
        <?php echo $content; ?>
</body>
</html>