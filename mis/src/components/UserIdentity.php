<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
		$r = uc_user_login($this->username, $this->password);
		if($r[0] == -1)
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($r[0] == -2)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		elseif($r[0] > 0)

		{
			$this->errorCode=self::ERROR_NONE;

			$agent   = $_SERVER['HTTP_USER_AGENT'];
			$ip      = $_SERVER['REMOTE_ADDR'];

			$authkey = md5($ip.$agent.UC_KEY);
			$check   = substr(md5($ip.$agent), 0, 8);
			$sid     = rawurlencode(uc_authcode("$this->username\t$check", 'ENCODE', $authkey, 1800));
			/*
			$r       = uc_get_user($this->username);
			if($r)
			{
				$_SESSION['__id'] = $r[0];
			}
			$_SESSION['__name'] = $this->username;
			*/

			$uid    = $r[0];
			$admin  = new Admins();
			$ad = $admin->get($this->username);
			if(!empty($ad['allowadmincp']))
			{
				$admin->updatestat($uid, $ad['uid'], $ad['adminid']);
			}
			else
			{
				return false;
			}

			setcookie('sid', $sid, $_SERVER['REQUEST_TIME']+1800, '/');
		}

		else
			$this->errorCode=self::ERROR_PASSWORD_INVALID;


		return !$this->errorCode;
	}
}
