<?php
class Util{
	public static function sqlUpdate($args){
		$str = '';
		foreach ($args as $k => $v){
			$str .= "`$k`='$v',";
		}
		return rtrim($str,',');
	}
	public static function easyPost($url, $data) {
		$ch = curl_init ( $url );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, array ( 'Content-Type: application/json'));
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		return $result;
	}
	
	public static function easyPosts($url, $data) {
		$ch = curl_init ( $url );
		curl_setopt ( $ch, CURLOPT_POST, 1 );
		curl_setopt ( $ch, CURLOPT_CUSTOMREQUEST, "POST" );
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		return $result;
	}
	
	public static function easyGet($url , $type = 'http') {
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $url );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		if($type === 'https'){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}
		curl_setopt ( $ch, CURLOPT_HEADER, 0 );
		$result = curl_exec ( $ch );
		curl_close ( $ch );
		return $result;
	}
}