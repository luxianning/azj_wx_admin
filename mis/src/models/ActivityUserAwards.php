<?php

/**
 * This is the model class for table "cms_huodong".
 *
 * The followings are the available columns in table 'cms_huodong':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $img1
 * @property string $date 
 * @property integer $upper
 * @property string $author
 */
class ActivityUserAwards extends CActiveRecord {
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'activity_user_awards';
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
		);
	}
	public function attributeLabels() {
		return array (
	'subscribe'=>'关注数',
	'help_times_new'=>'助力数',
	'help_times'=>'助力数'
			     );
	}
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'activity' => array(self::BELONGS_TO, 'Activity','', 'on'=>'a.id=t.activity_id'),
				'user'=>array(self::BELONGS_TO, 'Users', '', 'on'=>'u.id=t.user_id'),
				'award'=>array(self::BELONGS_TO, 'EventsAwards', '', 'on'=>'aw.id=t.award_id'),
				'activity_user'=>array(self::BELONGS_TO, 'ActivityUser', '', 'on'=>'au.user_id=t.user_id and au.activity_id=t.activity_id'),
			    );
	}

	public function del($id) {
		$sql = "DELETE FROM `activity_user_awards` WHERE activity_id=$id";
		return Yii::app ()->db->createCommand ( $sql )->execute ();
	} 

	public function saveActivityUserAwards($args) {
		Yii::app ()->db->createCommand ()->insert ( 'activity_user_awards', $args );
		return Yii::app()->db->getLastInsertID();
	}

	public function getAwardUsers($id) 
	{
		$id = intval ( $id );
		$criteria = new CDbCriteria ();
		$criteria->condition = " t.`activity_id`=".$id;
		$criteria->select = 'address,mobile,user_id,idno,realname';
		$criteria->with = array(
				"activity_user"=>array(
					//"alias"=>'au',"select"=>"subscribe,help_times_new,help_times"),
					"alias"=>'au',"select"=>"subscribe,help_times", "order"=>"help_times desc"),
				"user"=>array(
					"alias"=>'u','select'=>'nickname,mobile,sex'
					),
				"award"=>array(
					"alias"=>'aw','select'=>'title'
					)
				); 
		$dataProvider = new CActiveDataProvider ( $this, array (
					'criteria' => $criteria
					) );
		return $dataProvider;
	}
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *        	active record class name.
	 * @return CmsHuodong the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}
