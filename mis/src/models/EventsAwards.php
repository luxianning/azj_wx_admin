<?php

/**
 * This is the model class for table "cms_huodong".
 *
 * The followings are the available columns in table 'cms_huodong':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $img1
 * @property string $date 
 * @property integer $upper
 * @property string $author
 */
class EventsAwards extends CActiveRecord {
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'award';
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
		);
	}
	public function attributeLabels() {
	    return array(
            'id' => 'ID',
	    );
	}
	public function relations()
	{
		return array(
				'activity_user_awards'=>array(self::HAS_MANY, 'ActivityUserAwards', 'award_id'),
			    );
	}
	public function contentByActivity($id){
	$sql = "SELECT * FROM `award` where activity_id=$id order by id asc";
	return Yii::app ()->db->createCommand ( $sql )->queryAll ();
	}
	public function newAward($args) {
		$sql = "select id FROM `award` WHERE activity_id=".$args['activity_id']." and title='".$args['title']."'";
                $dt = Yii::app ()->db->createCommand ( $sql )->queryRow ();
		if($args['activity_id'] > 0 && isset($dt['id']))
		{
			$set = Util::sqlUpdate ( $args );
			$sql = "UPDATE `award` set $set where id=".$dt['id'];
			return Yii::app ()->db->createCommand ( $sql )->execute ();
		}
 		else 
			return Yii::app ()->db->createCommand ()->insert ( 'award', $args );
	}
	public function delByActivity($aid) {
		$sql = "DELETE FROM `award` WHERE activity_id=$aid";
		return Yii::app ()->db->createCommand ( $sql )->execute ();
	} 
	public function getEventsAwards($id) { 
		$id = intval ( $id );

		$criteria = new CDbCriteria ();
		$criteria->alias = 'a'; 
		$criteria->join = 'LEFT JOIN `activity` at ON a.`activity_id`=at.`id`';
		$criteria->condition = 'at.`id`=' . $id;
		$criteria->select = 'a.*';

		$dataProvider = new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
		return $dataProvider;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *        	active record class name.
	 * @return CmsHuodong the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}
