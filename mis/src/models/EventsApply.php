<?php

/**
 * This is the model class for table "cms_huodong".
 *
 * The followings are the available columns in table 'cms_huodong':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $img1
 * @property string $date 
 * @property integer $upper
 * @property string $author
 */
class EventsApply extends CActiveRecord {
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'pre_huodong_map';
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
		);
	}
	public function attributeLabels() {
	    return array(
            'id' => 'ID',
            'openid' => 'Openid',
            'name' => '会员姓名',
            'mtel' => '会员手机',
            'email' => '会员email',
            'sex' => '会员性别',
            'age' => '会员年龄',
            'edu' => '会员学历',
            'habby' => '会员爱好',
            'wx_subscribe' => '是否关注公众号',
            'wx_nickname' => '用户昵称',
            'wx_sex' => '用户性别',
            'wx_city' => '用户所在城市',
            'wx_country' => '国家',
            'wx_province' => '省份',
            'wx_language' => '语言',
            'wx_headimgurl' => '头像',
            'wx_subscribe_time' => '用户关注时间',
            'event_times' => '报名次数',
            'sign_times' => '每日签到',
            'event_join_times' => '参加次数', 
		);
	}
	public function getApplyUsers($id) { 
		$id = intval ( $id );
		$sql = "SELECT h.`title`,u.* FROM `pre_huodong_map` hm
				LEFT JOIN `pre_user` u
				ON hm.`openid` = u.`openid`
				LEFT JOIN `cms_huodong` h
				ON hm.`huodongid` = h.`id`
				WHERE hm.`huodongid` = $id";
		$data = Yii::app ()->db->createCommand ( $sql )->queryAll ();
		if (empty ( $data ))
			return FALSE;
		$this->attributes = array(
            'id' => 'ID',
            'openid' => 'Openid',
            'name' => '会员姓名',
            'mtel' => '会员手机',
            'email' => '会员email',
            'sex' => '会员性别',
            'age' => '会员年龄',
            'edu' => '会员学历',
            'habby' => '会员爱好',
            'wx_subscribe' => '是否关注公众号',
            'wx_nickname' => '用户昵称',
            'wx_sex' => '用户性别',
            'wx_city' => '用户所在城市',
            'wx_country' => '国家',
            'wx_province' => '省份',
            'wx_language' => '语言',
            'wx_headimgurl' => '头像',
            'wx_subscribe_time' => '用户关注时间',
            'event_times' => '报名次数',
            'sign_times' => '每日签到',
            'event_join_times' => '参加次数', 
		);

		$dataProvider = new CArrayDataProvider ( $data, array (
				'id' => 'ApplyUsers' 
		// 'sort' => array(
		// 'attributes' => array(
		// 'id', 'title', 'content'
		// ),
		// ),
		// 'pagination' => array(
		// 'pageSize' => 10,
		// ),
				) );
		$criteria = new CDbCriteria ();
        $criteria->alias = 'hdm'; 
        $criteria->join = 'LEFT JOIN `cms_huodong` hd ON hdm.`huodongid`=hd.`id`';
        $criteria->join .= ' LEFT JOIN `pre_user` u ON hdm.`openid`=u.`openid`';
        $criteria->condition = 'hd.`id`=' . $id;
        $criteria->select = 'hd.`title`, u.*';
		$dataProvider = new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
        
		return $dataProvider;
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *        	active record class name.
	 * @return CmsHuodong the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}
