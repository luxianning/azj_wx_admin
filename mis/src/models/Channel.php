<?php

/**
 * This is the model class for table "activity".
 *
 * The followings are the available columns in table 'activity':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $img1
 * @property string $date 
 * @property integer $upper
 * @property string $author
 */
class Channel extends CActiveRecord {
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'channel_detail';
	}

	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array( 
 				//'channel_stat'=>array(self::HAS_MANY, 'ChannelStat', 'channel'),
				'channel_stat'=>array(self::BELONGS_TO, 'ChannelStat','', 'on'=>'cs.channel=t.channel'),
			    );
	}

	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
				array (
						'title, content, img1, pubdate, upper, author',
						'required' 
				),
				array (
						'upper',
						'numerical',
						'integerOnly' => true 
				),
				array (
						'title, img1',
						'length',
						'max' => 255 
				),
				array (
						'author',
						'length',
						'max' => 128 
				),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array (
						'id, title, content, thumb, date, upper, author',
						'safe',
						'on' => 'search' 
				) 
		);
	}
	public function attributeLabels() {
		return array (
				'title' => '活动名称',
				'keyword' => '关键词',
				'visits' => '访问量',
				'apply_num' => '报名参加人数',
				'new_fans_num' => '新增粉丝数',
				'status' => '状态',
		);
	}
	public function search($date) {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria ();
		if(!isset($_GET['Events_sort'])){
			$criteria->order='channel DESC';
		}
		$criteria->select = 'count(*) as id,channel,openid,name,img';
		$criteria->compare ( 'id', $this->id );
		$criteria->addSearchCondition('create_time',$date);
		$criteria->group  = 'channel';
		$criteria->addCondition("type=1");
/*
		$criteria->with = array(
				"channel_stat"=>array(
					"alias"=>'cs','select'=>'subscribe'
					)
				);
*/		
		return new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
	}
	public function getApplyUsers($id) { 
		$id = intval ( $id );
		$sql = "SELECT h.`title`,u.* FROM `pre_huodong_map` hm
				LEFT JOIN `pre_user` u
				ON hm.`openid` = u.`openid`
				LEFT JOIN `activity` h
				ON hm.`huodongid` = h.`id`
				WHERE hm.`huodongid` = $id";
		$data = Yii::app ()->db->createCommand ( $sql )->queryAll ();
		if (empty ( $data ))
			return FALSE;
		$this->attributes = array(
            'id' => 'ID',
            'openid' => 'Openid',
            'name' => '会员姓名',
            'mtel' => '会员手机',
            'email' => '会员email',
            'sex' => '会员性别',
            'age' => '会员年龄',
            'edu' => '会员学历',
            'habby' => '会员爱好',
            'wx_subscribe' => '是否关注公众号',
            'wx_nickname' => '用户昵称',
            'wx_sex' => '用户性别',
            'wx_city' => '用户所在城市',
            'wx_country' => '国家',
            'wx_province' => '省份',
            'wx_language' => '语言',
            'wx_headimgurl' => '头像',
            'wx_subscribe_time' => '用户关注时间',
            'event_times' => '报名次数',
            'sign_times' => '每日签到',
            'event_join_times' => '参加次数', 
		);

		$dataProvider = new CArrayDataProvider ( $data, array (
				'id' => 'ApplyUsers' 
		// 'sort' => array(
		// 'attributes' => array(
		// 'id', 'title', 'content'
		// ),
		// ),
		// 'pagination' => array(
		// 'pageSize' => 10,
		// ),
				) );
		$criteria = new CDbCriteria ();
        $criteria->alias = 'hd'; 
        $criteria->join = 'LEFT JOIN `pre_huodong_map` hdm ON hdm.`huodongid`=hd.`id`';
        $criteria->join .= ' LEFT JOIN `pre_user` u ON hdm.`openid`=u.`openid`';
        $criteria->condition = 'hd.`id`=' . $id;
        $criteria->select = 'hd.`title`, u.*';
		$dataProvider = new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
        
        var_dump($dataProvider);

		return $dataProvider;
	}
	
	// public function getList(){
	// $sql = "SELECT * FROM activity ORDER BY `pubdate` desc LIMIT 100";
	// return Yii::app ()->db->createCommand ( $sql )->queryall (true);
	// }
	public function saveEvent($args) {
		Yii::app ()->db->createCommand ()->insert ( 'activity', $args );
		return Yii::app()->db->getLastInsertID();
	}
	public function del($id) {
		$sql = "DELETE FROM `activity` WHERE id=$id";
		return Yii::app ()->db->createCommand ( $sql )->execute ();
	} 
	public function content($id){
		$sql = "SELECT * FROM `activity` where id=$id";
		return Yii::app ()->db->createCommand ( $sql )->queryRow ();
	}
	public function getAllPushedCount($start_date, $end_date){
		$sql = "SELECT count(*) as cnt FROM `activity` where push=1 and push_time>='$start_date' and push_time<='$end_date'";
		return Yii::app ()->db->createCommand ( $sql )->queryRow ()['cnt'];
	}
	public function updateEvent($args, $id) {
		$set = Util::sqlUpdate ( $args );
		$sql = "UPDATE `activity` set $set where id=$id";
		return Yii::app ()->db->createCommand ( $sql )->execute ();
	}
	// public function deluser($id){
	// $sql = "DELETE FROM `pre_user` WHERE id=$id";
	// return Yii::app()->db->createCommand($sql)->execute();
	// }
	// public function getUlist(){
	// $sql = "SELECT u.*,v.`count` FROM `pre_user` u
	// LEFT JOIN `pre_vipsign` v
	// ON u.`openid`=v.`openid` LIMIT 1000";
	// return Yii::app ()->db->createCommand ( $sql )->queryAll() ;
	// }
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *        	active record class name.
	 * @return CmsHuodong the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}
