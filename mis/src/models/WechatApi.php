<?php
class WechatApi {
	public $type = '';
	public function __construct() {
		$this->type = $_POST ['type'];
	}
	public function doApi() {
		$type = strtolower ( $this->type );
		switch ($type) {
			case 'mklist' :
				$this->mkList ();
				break;
			default :
				return FALSE;
		}
	}
	public function mkList() {
		$cb = urlencode(URL . 'web_index.php?contr=Vipsign/vipsign');
		$list = array (
				'button' => array (
						0 => array (
								'name' => urlencode ( '活动报名' ),
								'sub_button' => array (
										0 => array (
												'type' => 'view',
												'name' => urlencode ( '申请会员' ),
												'url' => 'http://182.92.158.7/wx_demo/web_index.php?contr=Login/login' 
										),
										1 => array (
												'type' => 'click',
												'name' => urlencode ( '查看活动' ),
												'key' => 'huodong'
										),
										2 => array (
												'type' => 'view',
												'name' => urlencode ( '每日签到' ),
												'url' => URL . 'web_index.php?contr=Vipsign/vipsign_jump'
										),
										3 => array (
												'type' => 'click',
												'name' => urlencode ( '关于我们' ),
												'key' => 'hongmihui'
										)
								) 
						),
						1 => array (
								'type' => 'view',
								'name' => urlencode ( '红迷家园' ),
								'url' => 'http://wsq.qq.com/reflow/263005961'
						),
						2 => array (
								'type' => 'view',
								'name' => urlencode ( '红迷商城' ),
								'url' => 'http://wd.koudai.com/s/168732530?wfr=c'
						),
				),
		);
		$url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . $this->getToken ();
		$list = urldecode ( json_encode ( $list ) );
		$rs = easyPost ( $url, $list );
		echo $rs;
	}
	private function getToken() {
		$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . AppId . '&secret=' . AppSecret;
		$token = easyGet ( $url );
		$token = json_decode ( $token, TRUE );
		return $token ['access_token'];
	}
}
