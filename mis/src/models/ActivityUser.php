<?php

/**
 * This is the model class for table "cms_huodong".
 *
 * The followings are the available columns in table 'cms_huodong':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $img1
 * @property string $date 
 * @property integer $upper
 * @property string $author
 */
class ActivityUser extends CActiveRecord {
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'activity_user';
	}
	
	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
		);
	}
	public function attributeLabels() {
		return array (
	'subscribe'=>'关注数',
	'help_times_new'=>'助力数',
	'help_times'=>'助力数'
			     );
	}
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'activity' => array(self::BELONGS_TO, 'Activity','', 'on'=>'a.id=t.activity_id'),
				'user'=>array(self::BELONGS_TO, 'Users', '', 'on'=>'u.id=t.user_id'),
				'activity_user_awards'=>array(self::BELONGS_TO, 'ActivityUserAwards', '', 'on'=>'activity_user_awards.user_id=t.user_id and activity_user_awards.activity_id=t.activity_id'),
			    );
	}
	public function updateActivityUser($id, $args) {
		$set = Util::sqlUpdate ( $args );
		$sql = "UPDATE `activity_user` set $set where id=$id";
		return Yii::app ()->db->createCommand ( $sql )->execute ();
	}
	public function getSignUsers($id) 
	{
		$id = intval ( $id );

		$criteria = new CDbCriteria ();
		$criteria->condition = " t.`activity_id`=".$id;
		$criteria->select = 't.id,t.subscribe,t.help_times_new,t.help_times,t.user_id,t.apply_time,t.user_remark';
		$criteria->with = array(
				"activity"=>array(
					"alias"=>'a',"select"=>"title"),
				"user"=>array(
					"alias"=>'u','select'=>'nickname,address,mobile,sex'
					)
				); 
		//$criteria->order = 't.help_times DESC';
		$dataProvider = new CActiveDataProvider ( $this, array (
					'pagination'=>false,
					'criteria' => $criteria
					) );
		//$data = Yii::app()->db->createCommand('select title from activity where id='.$id)->queryAll();
		//$dataProvider->setData($data);
		return $dataProvider;
	}
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
	}
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *        	active record class name.
	 * @return CmsHuodong the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}
