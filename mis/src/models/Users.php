 <?php

/**
 * This is the model class for table "pre_user".
 *
 * The followings are the available columns in table 'pre_user':
 * @property integer $id
 * @property string $openid
 * @property string $name
 * @property integer $mtel
 * @property string $email
 * @property string $sex
 * @property integer $age
 * @property string $edu
 * @property string $habby
 * @property integer $wx_subscribe
 * @property string $wx_nickname
 * @property integer $wx_sex
 * @property string $wx_city
 * @property string $wx_country
 * @property string $wx_province
 * @property string $wx_language
 * @property string $wx_headimgurl
 * @property integer $wx_subscribe_time
 * @property integer $event_times
 * @property integer $sign_times
 * @property string $last_sign_date
 * @property integer $event_join_times
 */
class Users extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'user';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('openid, name, mtel, email, sex, age, edu, habby, wx_subscribe, wx_nickname, wx_sex, wx_city, wx_country, wx_province, wx_language, wx_headimgurl, wx_subscribe_time, event_times, sign_times, last_sign_date', 'required'),
            array('mtel, age, wx_subscribe, wx_sex, wx_subscribe_time, event_times, sign_times, event_join_times', 'numerical', 'integerOnly'=>true),
            array('openid, wx_nickname, wx_city, wx_country, wx_province', 'length', 'max'=>64),
            array('name', 'length', 'max'=>30),
            array('email', 'length', 'max'=>128),
            array('sex', 'length', 'max'=>1),
            array('edu', 'length', 'max'=>12),
            array('habby', 'length', 'max'=>255),
            array('wx_language', 'length', 'max'=>32),
            array('wx_headimgurl', 'length', 'max'=>512),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('title,id, openid, name, mtel, email, sex, age, edu, habby, wx_subscribe, wx_nickname, wx_sex, wx_city, wx_country, wx_province, wx_language, wx_headimgurl, wx_subscribe_time, event_times, sign_times, last_sign_date, event_join_times', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
	    return array(
			    'activity_user'=>array(self::HAS_MANY, 'ActivityUser', 'user_id'),
			    'activity_user_awards'=>array(self::HAS_MANY, 'ActivityUserAwards', 'user_id'),
			);
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'openid' => 'Openid',
            'name' => '会员姓名',
            'mtel' => '会员手机',
            'email' => '会员email',
            'sex' => '会员性别',
            'age' => '会员年龄',
            'edu' => '会员学历',
            'habby' => '会员爱好',
            'wx_subscribe' => '是否关注公众号',
            'wx_nickname' => '用户昵称',
            'wx_sex' => '用户性别',
            'wx_city' => '用户所在城市',
            'addr' => '邮寄地址',
            'wx_country' => '国家',
            'wx_province' => '省份',
            'wx_language' => '语言',
            'wx_headimgurl' => '头像',
            'wx_subscribe_time' => '用户关注时间',
            'event_times' => '报名次数',
            'sign_times' => '每日签到',
            'event_join_times' => '参加次数', 
		'title'=>'',
        );   
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;
		
        $criteria->addCondition("isdel<>1");
        $criteria->addCondition("name<>''");
        $criteria->compare('id',$this->id);
        $criteria->compare('openid',$this->openid,true);
        $criteria->compare('name',$this->name,true);
        $criteria->compare('mtel',$this->mtel);
        $criteria->compare('email',$this->email,true);
        $criteria->compare('sex',$this->sex,true);
        $criteria->compare('age',$this->age);
        $criteria->compare('edu',$this->edu,true);
        $criteria->compare('habby',$this->habby,true);
        $criteria->compare('wx_subscribe',$this->wx_subscribe);
        $criteria->compare('wx_nickname',$this->wx_nickname,true);
        $criteria->compare('wx_sex',$this->wx_sex);
        $criteria->compare('wx_city',$this->wx_city,true);
        $criteria->compare('wx_country',$this->wx_country,true);
        $criteria->compare('wx_province',$this->wx_province,true);
        $criteria->compare('wx_language',$this->wx_language,true);
        $criteria->compare('wx_headimgurl',$this->wx_headimgurl,true);
        $criteria->compare('wx_subscribe_time',$this->wx_subscribe_time);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
	public function deleteUser($id){
		$id = intval($id);
		$sql = "UPDATE `pre_user` set isdel=1 where id=$id";
		return Yii::app ()->db->createCommand ( $sql )->execute();
	}

    public function getApplyUsers($id) 
    {
        $id = intval ( $id );
        $criteria = new CDbCriteria ();
        $criteria->join = 'RIGHT JOIN `pre_huodong_map` hdm ON hdm.`openid`=t.`openid`';
        $criteria->join .= ' LEFT JOIN `cms_huodong` hd ON hdm.`huodongid`=hd.`id`';
        $criteria->condition = 'hdm.`huodongid`=' . $id;
        $dataProvider = new CActiveDataProvider ( $this, array (
                                'criteria' => $criteria

                ) );
        return $dataProvider;
    }

    public function getSignUsers($id) 
    {
        $id = intval ( $id );
        $criteria = new CDbCriteria ();
	$criteria->alias = 'u';

	$criteria->join = 'RIGHT JOIN `activity_user` hdm ON hdm.`user_id`=u.`id`';
        $criteria->join .= ' LEFT JOIN `activity` hd ON hdm.`activity_id`=hd.`id`';
        $criteria->condition = 'hdm.`activity_id`=' . $id;
	$criteria->select = 'sex, hd.id,hd.title';
        $dataProvider = new CActiveDataProvider ( $this, array (
                                'criteria' => $criteria
                ) );
        return $dataProvider;
    }
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Users the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
