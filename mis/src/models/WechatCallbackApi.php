<?php
define('AppId' , Yii::app()->params->AppId);
define('AppSecret',Yii::app()->params->AppSecret);
class WechatCallbackApi {
	public $postArr = array ();
	public $postStr = array ();
	public function __construct() {
	}
	public function getTocken($code){
		$url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid='.AppId.'&secret='.AppSecret.'&code='.$code.'&grant_type=authorization_code';
		return json_decode(Util::easyGet($url , 'https') , TRUE );
	}
	public function responseMsg( $fromUser , $toUser , $msg, $type = 'text') {
		switch ($type) {
			case 'text' :
				$tpl = $this->getTpl ( 'text' );
				$msg = sprintf($tpl,$fromUser,$toUser,TS,'text', $msg);
				break;
			default :
				rlog ( 'wrong type', 'error' );
				return FALSE;
		}
		exit($msg);
	}
	public function getQrTicketLimit($id){
		$token = $this->getToken();
		$url = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=".$token;
		$args = '{"action_name": "QR_LIMIT_SCENE", "action_info": {"scene": {"scene_id": '.$id.'}}}';
		$rs = json_decode(Util::easyPosts($url, $args),true);
		$ticket = urlencode($rs['ticket']);
		$qrUrl = 'https://mp.weixin.qq.com/cgi-bin/showqrcode?ticket='.$ticket;
		return array('qrurl'=>$qrUrl,'ticket'=>$ticket);
	}
	private function getTpl($type) {
		switch ($type) {
			case 'text' :
				return "<xml>
						<ToUserName><![CDATA[%s]]></ToUserName>
						<FromUserName><![CDATA[%s]]></FromUserName>
						<CreateTime>%s</CreateTime>
						<MsgType><![CDATA[%s]]></MsgType>
						<Content><![CDATA[%s]]></Content>
						<FuncFlag>0</FuncFlag>
						</xml>";
			default :
				return FALSE;
		}
	}
	
	private function checkSignature() {
		$signature = $_GET ["signature"];
		$timestamp = $_GET ["timestamp"];
		$nonce = $_GET ["nonce"];
		
		$token = TOKEN;
		$tmpArr = array (
				$token,
				$timestamp,
				$nonce 
		);
		sort ( $tmpArr, SORT_STRING );
		$tmpStr = implode ( $tmpArr );
		$tmpStr = sha1 ( $tmpStr );
		
		if ($tmpStr == $signature) {
			return true;
		} else {
			return false;
		}
	}
	
	public function getUserInfo($code){
		$tocken = $this -> getTocken($code);
		$accessToken = $tocken['access_token'];
		$refreshToken = $tocken['refresh_token'];
		$openId = $tocken['openid'];
		$url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$accessToken.'&openid='.$openId.'&lang=zh_CN';
		$rs = json_decode(Util::easyGet($url , 'https') , TRUE );
		$rs ['refresh_token'] = $refreshToken;
		return $rs;
	}
	
	public function getTockenByRefr($refreshToken){
		$url = "https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=".AppId."&grant_type=refresh_token&refresh_token=$refreshToken";
		return json_decode(Util::easyGet($url , 'https') , TRUE );
	}
	
	public function sendMsg($openId , $msg){
		$accessToken = $this->getToken();
		$data = '{
			    "touser":"'.$openId.'",
			    "msgtype":"text",
			    "text":
			    {
			         "content":"'.$msg.'"
			    }
			}';
		$url = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=$accessToken";
		return easyPosts($url , $data);
	}
	
	private function getToken() {
		$url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=' . AppId . '&secret=' . AppSecret;
		$token = Util::easyGet ( $url );
		$token = json_decode ( $token, TRUE );
		return $token ['access_token'];
	}
	//获取永久二维码ticket
	
}