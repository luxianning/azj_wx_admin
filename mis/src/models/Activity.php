<?php

/**
 * This is the model class for table "cms_huodong".
 *
 * The followings are the available columns in table 'cms_huodong':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $img1
 * @property string $date 
 * @property integer $upper
 * @property string $author
 */
class Activity extends CActiveRecord {
	/**
	 *
	 * @return string the associated database table name
	 */
	public function tableName() {
		return 'activity';
	}
	
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
				'activity_user'=>array(self::HAS_MANY, 'ActivityUser', 'activity_id'),
				'activity_user_awards'=>array(self::HAS_MANY, 'ActivityUserAwards', 'activity_id'),
			    );
	}

	/**
	 *
	 * @return array validation rules for model attributes.
	 */
	public function rules() {
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array (
				array (
						'title, keyword, award_title',
						'required',
						'allowEmpty'=>false
				),
				array (
						'upper',
						'numerical',
						'integerOnly' => true 
				),
				array (
						'title, img1',
						'length',
						'max' => 255 
				),
				array (
						'author',
						'length',
						'max' => 128 
				),
				// The following rule is used by search().
				// @todo Please remove those attributes that should not be searched.
				array (
						'id, title, content, thumb, date, upper, author',
						'safe',
						'on' => 'search' 
				) 
		);
	}
	public function attributeLabels() {
		return array (
				'id' => '活动编号',
				'title' => '活动名称',
				'des' => '活动摘要',
				'startdate' => '开始时间',
				'enddate' => '结束时间',
				'place' => '活动地点',
				'upper' => '活动人数',
				'st' => '活动状态',
				'author' => '发布者',
                'mtime' => '编辑时间',
		);
	}
	public function search() {
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria = new CDbCriteria ();
		if(!isset($_GET['Events_sort'])){
			$criteria->order='id DESC';
		}
		$criteria->compare ( 'id', $this->id );
		$criteria->compare ( 'title', $this->title, true );
		$criteria->compare ( 'content', $this->content, true );
		$criteria->compare ( 'thumb', $this->thumb, true );
		$criteria->compare ( 'upper', $this->upper );
		$criteria->compare ( 'author', $this->author, true );
		
		return new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
	}
	public function getApplyUsers($id) { 
		$id = intval ( $id );
		$sql = "SELECT h.`title`,u.* FROM `pre_huodong_map` hm
				LEFT JOIN `pre_user` u
				ON hm.`openid` = u.`openid`
				LEFT JOIN `cms_huodong` h
				ON hm.`huodongid` = h.`id`
				WHERE hm.`huodongid` = $id";
		$data = Yii::app ()->db->createCommand ( $sql )->queryAll ();
		if (empty ( $data ))
			return FALSE;
		$this->attributes = array(
            'id' => 'ID',
            'openid' => 'Openid',
            'name' => '会员姓名',
            'mtel' => '会员手机',
            'email' => '会员email',
            'sex' => '会员性别',
            'age' => '会员年龄',
            'edu' => '会员学历',
            'habby' => '会员爱好',
            'wx_subscribe' => '是否关注公众号',
            'wx_nickname' => '用户昵称',
            'wx_sex' => '用户性别',
            'wx_city' => '用户所在城市',
            'wx_country' => '国家',
            'wx_province' => '省份',
            'wx_language' => '语言',
            'wx_headimgurl' => '头像',
            'wx_subscribe_time' => '用户关注时间',
            'event_times' => '报名次数',
            'sign_times' => '每日签到',
            'event_join_times' => '参加次数', 
		);

		$dataProvider = new CArrayDataProvider ( $data, array (
				'id' => 'ApplyUsers' 
		// 'sort' => array(
		// 'attributes' => array(
		// 'id', 'title', 'content'
		// ),
		// ),
		// 'pagination' => array(
		// 'pageSize' => 10,
		// ),
				) );
		$criteria = new CDbCriteria ();
        $criteria->alias = 'hd'; 
        $criteria->join = 'LEFT JOIN `pre_huodong_map` hdm ON hdm.`huodongid`=hd.`id`';
        $criteria->join .= ' LEFT JOIN `pre_user` u ON hdm.`openid`=u.`openid`';
        $criteria->condition = 'hd.`id`=' . $id;
        $criteria->select = 'hd.`title`, u.*';
		$dataProvider = new CActiveDataProvider ( $this, array (
				'criteria' => $criteria 
		) );
        
        var_dump($dataProvider);

		return $dataProvider;
	}
	
	// public function getList(){
	// $sql = "SELECT * FROM cms_huodong ORDER BY `pubdate` desc LIMIT 100";
	// return Yii::app ()->db->createCommand ( $sql )->queryall (true);
	// }
	public function saveEvent($args) {
		return Yii::app ()->db->createCommand ()->insert ( 'cms_huodong', $args );
	}
	public function del($id) {
		$sql = "DELETE FROM `cms_huodong` WHERE id=$id";
		return Yii::app ()->db->createCommand ( $sql )->execute ();
	} 
	public function content($id){
	$sql = "SELECT * FROM `cms_huodong` where id=$id";
	return Yii::app ()->db->createCommand ( $sql )->queryRow ();
	}
	public function updateEvent($args, $id) {
		$set = Util::sqlUpdate ( $args );
		$sql = "UPDATE `cms_huodong` set $set where id=$id";
		return Yii::app ()->db->createCommand ( $sql )->execute ();
	}
	// public function deluser($id){
	// $sql = "DELETE FROM `pre_user` WHERE id=$id";
	// return Yii::app()->db->createCommand($sql)->execute();
	// }
	// public function getUlist(){
	// $sql = "SELECT u.*,v.`count` FROM `pre_user` u
	// LEFT JOIN `pre_vipsign` v
	// ON u.`openid`=v.`openid` LIMIT 1000";
	// return Yii::app ()->db->createCommand ( $sql )->queryAll() ;
	// }
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 *
	 * @param string $className
	 *        	active record class name.
	 * @return CmsHuodong the static model class
	 */
	public static function model($className = __CLASS__) {
		return parent::model ( $className );
	}
}
