<?php
class DefaultController extends Controller {
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array (
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha' => array (
						'class' => 'CCaptchaAction',
						'backColor' => 0xFFFFFF 
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page' => array (
						'class' => 'CViewAction' 
				) 
		);
	}
	public function actionUcode() {
		$uc = new UserCenter ();
		echo json_encode ( [ 
			'code' => $uc->uccode (),
				'time' => $_SERVER ['REQUEST_TIME'] 
		] );
	}
	public function actionZone() {
		$this->render ( 'zone', array () );
	} 
	public function actionIndex() {
		$this->render ( 'index', array () );
	}
	
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app ()->errorHandler->error) {
			
			if (Yii::app ()->request->isAjaxRequest)
				echo $error ['message'];
			else
				include dirname ( __DIR__ ) . "/views/default/error.php";
			// $this->render ( 'error', $error );
		}
	}
	
	/**
	 * Displays the login page
	 */
	public function actionLogin() {
		$model = new LoginForm ();
		// if it is ajax validation request
		if (isset ( $_POST ['ajax'] ) && $_POST ['ajax'] === 'login-form') {
			echo CActiveForm::validate ( $model );
			Yii::app ()->end ();
		}
		// collect user input data
		if (isset ( $_POST ['LoginForm'] )) {
			$model->attributes = $_POST ['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if ($model->validate () && $model->login ())
				$this->redirect ( Yii::app ()->user->returnUrl );
		}
		// display the login form
		// $this->render('login',array('model'=>$model));
		include dirname ( __DIR__ ) . "/views/default/login.php";
	}
	
	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout() {
		Yii::app ()->user->logout ();
		
		foreach ( $_COOKIE as $k => $v ) {
			setcookie ( $k, 'deleted', $_SERVER ['REQUEST_TIME'] - 3600, '/' );
		}
		
		$this->redirect ( Yii::app ()->homeUrl );
	}


	public function actionTttt()
	{
		$aa = [];
		$aa['xxxxxx'];
	}

}

