<?php
class UsersController extends Controller {
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array (
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha' => array (
						'class' => 'CCaptchaAction',
						'backColor' => 0xFFFFFF
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page' => array (
						'class' => 'CViewAction'
				)
		);
	}

	public function actionLists() {
		$model = new Users ();
		$dataProvider  = $model ->search();
		$this->render ( 'index', array (
				'dataProvider' => $dataProvider
		) );
	}

	public function actionExports()
	{
		$model = new Users ();
		$data = $model->findAll();

		$this->layout = '//layouts/export';

		header ( "Content-Type:application/vnd.ms-excel;charset=utf8" );
		header ( "Content-Disposition:attachment;filename=红迷会员.xls" );
		header ( "Pragma:no-cache" );
		header ( "Expires:0" );
		$this->render ( 'export', array
				(
				 'data' => $data
				)
			      );
	}

	public function actionDelete()
	{
		if(isset($_GET['id'])){
			$model = new Users ();
			$model->deleteUser($_GET['id']);
		}
		$this->redirect( array('Lists'));
	}

	public function loadModel($id)
	{
		$model=Users::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

}

