<?php
class MenuController extends Controller {
	
	public function actionAdmin() {
		$menu_file = DIR_ROOT.'/src/config/menu.json';

		if(isset($_POST['button_1_name']))
		{
			$menu = ['button'=>[]];
			$menu_row = [];
			$menu_row['name'] = $_POST['button_1_name'];
			if(isset($_POST['enable_sub_button_1']) && $_POST['enable_sub_button_1'] == 'on')
			{
				foreach($_POST['sub_button_1_name'] as $key=>$name)
				{
					if($name && $_POST['sub_button_1_link'][$key])
						$menu_row['sub_button'][] = ['type'=>'view','name'=>$name, 'url'=>$_POST['sub_button_1_link'][$key]];
				}
			}
			else
			{
				$menu_row['url'] = $_POST['button_1_link'];
				$menu_row['type'] = 'click';
			}
			$menu['button'][] = $menu_row;

			$menu_row = [];
			$menu_row['name'] = $_POST['button_2_name'];
			if(isset($_POST['enable_sub_button_2']) && $_POST['enable_sub_button_2'] == 'on')
			{
				foreach($_POST['sub_button_2_name'] as $key=>$name)
				{
					if($name && $_POST['sub_button_2_link'][$key])
						$menu_row['sub_button'][] = ['type'=>'view','name'=>$name, 'url'=>$_POST['sub_button_2_link'][$key]];
				}
			}
			else
			{
				$menu_row['url'] = $_POST['button_2_link'];
				$menu_row['type'] = 'click';
			}
			$menu['button'][] = $menu_row;

			$menu_row = [];
			$menu_row['name'] = $_POST['button_3_name'];
			if(isset($_POST['enable_sub_button_3']) && $_POST['enable_sub_button_3'] == 'on')
			{
				foreach($_POST['sub_button_3_name'] as $key=>$name)
				{
					if($name && $_POST['sub_button_3_link'][$key])
						$menu_row['sub_button'][] = ['type'=>'view','name'=>$name, 'url'=>$_POST['sub_button_3_link'][$key]];
				}
			}
			else
			{
				$menu_row['url'] = $_POST['button_3_link'];
				$menu_row['type'] = 'click';
			}
			$menu['button'][] = $menu_row;
			
			$menu_json = json_encode($menu, JSON_UNESCAPED_UNICODE);

			file_put_contents($menu_file, $menu_json);	

			$url = 'http://'.Yii::app()->params['app_host'] . "/index.php?m=activity&f=setMenu&backend=1&menu=".$menu_json;
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$dt = curl_exec($ch);
			curl_close($ch);
		}
		$menu = file_get_contents($menu_file);
		$menu = json_decode($menu, true);

		$this->render ( 'admin', array (
					'menu'=>$menu
					) );
	}

	function actionSend()
	{
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : 0;
		$ret = ['status'=>0,'data'=>[],'message'=>''];
		
		$first_day = date('Y-m-01 00:00:00');
		$now = date('Y-m-d H:i:s');

		$model = new Events();
		$count = $model->getAllPushedCount($first_day, $now);	
		if($count >= 4)
		{
			$ret['status'] = 1;
			$ret['message'] = '本月已经推送了4次';	
			echo json_encode($ret);
			exit;
		}
		
		$url = 'http://'.Yii::app()->params['app_host'] . "/index.php?m=activity&f=pushMessage&id=$id&d=json&backend=1";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$dt = curl_exec($ch);
		curl_close($ch);

		//$dt = file_get_contents($url);
		$dt = json_decode($dt, true);
		if(!empty($dt))
		{
			if($dt['errno'] == 0)
			{
				$model->updateEvent(['push'=>1,'push_time'=>$now], $id);
				$ret['status'] = 0;
			}
			else
			{
				$ret['status'] = 1;
				$ret['message'] = '返回错误码：'.$dt['errno'];
			}
			echo json_encode($ret);
			exit;
		}
		else 
		{
			$ret['status'] = 1;
			$ret['message'] = '调用群发消息接口失败';	
			echo json_encode($ret);
			exit;
		}
	}
}
