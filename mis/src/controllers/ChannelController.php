<?php
class ChannelController extends Controller {
	
	public function actionCreateQR(){
		$m = new Events ();
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : 0;
		if(empty($id)){
			echo "<script>alert('只有活动生成以后才可以产生二维码')</script>";
		}
		$url = 'http://'.Yii::app()->params['app_host'] . "/index.php?m=activity&f=detail&backend=1&id=".$id;
		QRcode::png($url);
		exit;
		//$file = "/upload/image/qrcode/{$id}.png";
		//QRcode::png("www.baidu.com", DIR_ROOT.$file, 'L', 4, 2);
$url = "http://wx-admin.cheyou360.com/mis".$file;
		//header("Location: ".$url);
		/*
		$wx = new  WechatCallbackApi(); 
		$rs = $wx->getQrTicketLimit($id);
		$m -> updateEvent(array('qrticket'=>$rs['ticket'] , ) , $id);
		header("Location: ".$rs['qrurl']);
		*/
		//echo "<script>javascript:location.href='".$QRUrl."'</script>";
	}
	
	
	
	/**
	 * Declares class-based actions.
	 */
	public function actions() {
		return array (
				// captcha action renders the CAPTCHA image displayed on the contact page
				'captcha' => array (
						'class' => 'CCaptchaAction',
						'backColor' => 0xFFFFFF 
				),
				// page action renders "static" pages stored under 'protected/views/site/pages'
				// They can be accessed via: index.php?r=site/page&view=FileName
				'page' => array (
						'class' => 'CViewAction' 
				) 
		);
	}

	public function actionEditHelpTimes() {
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : 0;
		$help_times = $_POST['help_times'];

		$model = new ActivityUser ();
		$model->updateActivityUser($id, ['help_times_new'=>$help_times,'help_times'=>$help_times]);
		$ret = ['status'=>0,'data'=>[],'message'=>''];
		echo json_encode($ret) ;
		exit;
	}

	public function actionAdmin() {
		$date = isset($_GET['date']) ? $_GET['date'] : date("Y-m-d");

		$model = new ChannelStat ();
		$dataProvider = $model->search ($date);
		$data = $dataProvider->getData();
		
		$modelCS = new Channel ();
		$inc = $modelCS->search($date);
		$channel_inc = [];
		foreach($channel_inc as $row)
		{
			$channel_inc[$row['channel']] = $row;
		}	

		foreach($data as $key=>$row)
		{
			$channel_data[$key] = $row;
			$channel_data[$key]['ticket'] = isset($channel_inc[$row['channel']]) ? $channel_inc[$row['channel']]['id']:0;
		}
		$dataProvider->setData($channel_data);

		$this->render ( 'admin', array (
					'dataProvider' => $dataProvider,
					'date'=>$date,
					) );
	}
	public function actionConfirmAward() {
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : 0;
		$award_model = new EventsAwards ();
		$awards = $award_model->getEventsAwards($id);
		$awards = $awards->getData();

		$model_au = new ActivityUser ();
		$activity_user = $model_au->getSignUsers($id);
		$activity_user = $activity_user->getData();

		$model_auw = new ActivityUserAwards ();
		$model_auw->del($id);

		$model_activity = new Events ();

		$award_opt = ['一等奖','二等奖','三等奖','四等奖','五等奖','六等奖','七等奖','八等奖','九等奖','十等奖','纪念奖'];
		
		usort($activity_user, function($a, $b){
			if($a['help_times'] == $b['help_times'])
			{
				if($a['apply_time'] > $b['apply_time']) 
					return 1;
				else
					return -1;
			}
			else
				return $a['help_times'] > $b['help_times'] ? -1:1;
		});

		$award_user = ['activity_id'=>$id];
		foreach($award_opt as $award_title)
		{
			foreach($awards as $award)
			{
				if($award_title == $award->title)
				{
					while($award->total--)
					{
						foreach($activity_user as $key=>$auser)
						{
							if($award_title == '纪念奖' && $auser->subscribe <= $award->subscribe_num)
								continue;
							else if($auser->help_times == 0) 
								continue;
							unset($activity_user[$key]);
							$award_user['activity_user_id'] = $auser->id;	
							$award_user['user_id'] = $auser->user_id;	
							$award_user['award_id'] = $award->id;	
							$award_user['create_time'] = date("Y-m-d H:i:s");	
							$dt = $model_auw->saveActivityUserAwards($award_user);
							break;
						}
					}
				}
			}
		}

		$url = 'http://'.Yii::app()->params['app_host'] . "/index.php?m=activity&f=pushAwardsMessage&id=$id&d=json&backend=1";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$dt = curl_exec($ch);
		curl_close($ch);
		$dt = json_decode($dt, true);
		if(!empty($dt))
		{
			if($dt['errno'] == 0)
			{
				$model_activity->updateEvent(['send_award_status'=>1], $id);
			}
			else
			{
				$model_auw->del($id);
				$ret['status'] = 1;
				$ret['message'] = '返回错误码，请重新发奖';
				echo json_encode($ret);
				exit;
			}
		}
		else 
		{
			$model_auw->del($id);
			$ret['status'] = 1;
			$ret['message'] = '调用群发消息接口失败，请重新发奖';	
			echo json_encode($ret);
			exit;
		}

		//header("Location: http://wx-admin.cheyou360.com/mis/index.php?r=events/award&id=$id");
		header("Location: ?r=events/award&id=$id");
	}

	public function actionCreate() {
		$model = new ChannelStat();
		//活动入库
		if (isset ( $_POST ['name'] ) ) {
			$date = date ('Y-m-d H:i:s');
			$update_data = [];
			//$update_data['channel'] = $_POST['channel'];
			$update_data['name'] = $_POST['name'];
			$update_data['create_time'] = $date;

			$url = 'http://'.Yii::app()->params['app_host'] . "/index.php?m=activity&f=addChannel&backend=1&name={$_POST['name']}&channel=";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$dt = curl_exec($ch);
			curl_close($ch);

			$this->redirect ( array (
					'admin' 
			) );
		}
		$this->render ( 'create');
	}
	public function actionPub() {
		$id = intval ( $_GET ['id'] );
		if ($id > 0) {
			$model = new Events ();
			$model->updateEvent (['status'=>1], $id );
		}
		$this->redirect ( array (
				'admin' 
		) );
	}

	public function actionDel() {
		$id = intval ( $_GET ['id'] );
		if ($id > 0) {
			$model = new Events ();
			$content = $model->content($id);
			$model->updateEvent (['status'=>2], $id );
		}
		$this->redirect ( array (
				'admin' 
		) );
	}
	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError() {
		if ($error = Yii::app ()->errorHandler->error) {
			if (Yii::app ()->request->isAjaxRequest)
				echo $error ['message'];
			else
				include dirname ( __DIR__ ) . "/views/events/error.php";
			// $this->render ( 'error', $error );
		}
	}
	public function actionAward() {
		$title = '';
		$model = new ActivityUserAwards ();
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : false;
		$page = isset ( $_GET['ActivityUserAwards_page'] ) ? (intval ( $_GET['ActivityUserAwards_page'])-1)  : 0;
		$ps = 10;

		if (empty ( $id )) {
			$this->redirect ( array (
					'admin' 
			) );
		}
		$dataProvider = $model->getAwardUsers ( $id );
		if(empty($dataProvider)){
			$data = array();
		}else{
			$data = $dataProvider->getData ();

			$help_times = [];
			foreach($data as $row)
			{
				$help_times[$row->id] = $row['activity_user']->help_times;
			}
			arsort($help_times);

			$i = 1;
			foreach($help_times as $_id=>$v)
			{
				foreach($data as $key=>$row)
				{
					if($row->id == $_id)
					{
						$data[$key]['user_id'] =  $page * $ps + $i++;
						break;
					}
				}
			}
			$dataProvider->setData ($data);

			$eventModel = new Events();
			$event = $eventModel->content($id);
			$title = $event ['title'];
		}
		if (isset ( $_GET ['export'] ) && 1 == $_GET ['export']) {
			$this->layout = '//layouts/export';
			header ( "Content-Type:application/vnd.ms-excel;charset=utf8" );
			header ( "Content-Disposition:attachment;filename=$title.xls" );
			header ( "Pragma:no-cache" );
			header ( "Expires:0" );
			$this->render (
					'activity_user_award_export', array
					(
					 'data' => $data
					)
				      );
		}
		$this->render ( 'activity_user_award', array
				(
				 'dataProvider' => $dataProvider,
				 'title' => $title,
				 'id' => $id 
				)
			      );
	}

	public function actionSign() {
		$title = '';
		$model = new ActivityUser ();
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : false;
		$page = isset ( $_GET['ActivityUser_page'] ) ? (intval ( $_GET['ActivityUser_page']) - 1)  : 0;
		$ps = 10;
		if (empty ( $id )) {
			$this->redirect ( array
					(
					 'admin' 
					) );
		}
		$dataProvider = $model->getSignUsers ( $id );
		if(empty($dataProvider)){
			$data = array();
		}else{
			$data = $dataProvider->getData ();

			$help_times = [];
			foreach($data as $row)
			{
				$help_times[$row->id] = [$row->help_times, $row->apply_time];
			}

			uasort($help_times, function($a, $b){
					if($a[0] == $b[0])
					{
						return $a[1] > $b[1] ? 1:-1;
					}
					else
						return $a[0] > $b[0] ? -1:1;
					});
			$i = 1;
			$sort_data = [];
			foreach($help_times as $_id=>$v)
			{
				foreach($data as $key=>$row)
				{
					if($row->id == $_id)
					{
						$sort_data[$key] = $row;
						$sort_data[$key]['user_id'] = $page * $ps + $i++;
						break;
					}
				}
			}
			$dataProvider->setData ($sort_data);
			$eventModel = new Events();
			$event = $eventModel->content($id);
			$title = $event ['title'];
			$end_time = $event['end_time'];
			$send_award_status = $event['send_award_status'];
		}
		if (isset ( $_GET ['export'] ) && 1 == $_GET ['export']) {
			$this->layout = '//layouts/export';
			header ( "Content-Type:application/vnd.ms-excel;charset=utf8" );
			header ( "Content-Disposition:attachment;filename=$title.xls" );
			header ( "Pragma:no-cache" );
			header ( "Expires:0" );
			$this->render ( 'sign_export', array
					(
					 'data' => $data
					)
				      );
		}
		$this->render ( 'sign', array
				(
				 'dataProvider' => $dataProvider,
				 'title' => $title,
				 'id' => $id ,
				 'end_time'=>$end_time,
				 'send_award_status'=>$send_award_status
				)
			      );
	}

	function actionSend()
	{
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : 0;
		$ret = ['status'=>0,'data'=>[],'message'=>''];
		
		$first_day = date('Y-m-01 00:00:00');
		$now = date('Y-m-d H:i:s');

		$model = new Events();
		$count = $model->getAllPushedCount($first_day, $now);	
		if($count >= 4)
		{
			$ret['status'] = 1;
			$ret['message'] = '本月已经推送了4次';	
			echo json_encode($ret);
			exit;
		}
		
		$url = 'http://'.Yii::app()->params['app_host'] . "/index.php?m=activity&f=pushMessage&id=$id&d=json&backend=1";
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		$dt = curl_exec($ch);
		curl_close($ch);

		//$dt = file_get_contents($url);
		$dt = json_decode($dt, true);
		if(!empty($dt))
		{
			if($dt['errno'] == 0)
			{
				$model->updateEvent(['push'=>1,'push_time'=>$now], $id);
				$ret['status'] = 0;
			}
			else
			{
				$ret['status'] = 1;
				$ret['message'] = '返回错误码：'.$dt['errno'];
			}
			echo json_encode($ret);
			exit;
		}
		else 
		{
			$ret['status'] = 1;
			$ret['message'] = '调用群发消息接口失败';	
			echo json_encode($ret);
			exit;
		}
	}
	public function actionClone() {
		$data ['content'] = array ();
		$data ['award'] = array ();
		$model = new Events ();
		$award_model = new EventsAwards ();
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : 0;
		$award_opt = ['一等奖','二等奖','三等奖','四等奖','五等奖','六等奖','七等奖','八等奖','九等奖','十等奖','纪念奖'];
		if ($id) {
			$content= $model->content ( $id ) ;
			$awards = $award_model->contentByActivity($id);
		}
		//活动入库
		if (!empty($content)) {
			$date = date ('Y-m-d H:i:s');
			$update_data = $content;
			unset($update_data['id']);
			$update_data['title'] = 'clone of '.$content['title'];
			$update_data['create_time'] = $date;
			$update_data['update_time'] = $date;
			$update_data['push'] = 0;
			$update_data['visits'] = 0;
			$update_data['status'] = 0;
			$update_data['apply_num'] = 0;
			$update_data['new_fans_num'] = 0;
			$update_data['send_award_status'] = 0;
			$update_data['push_time'] = $date;

			$id = $model->saveEvent ( $update_data);

			foreach($awards as $award)
			{
				unset($award['id']);
				$award['activity_id'] = $id;
				$award['create_time'] = $date;
				$award['update_time'] = $date;

				$award_model->newAward($award);
			}
		}
		$this->redirect ( array (
					'admin' 
					) );
	}
	function actionEditRemark()
	{
		$id = isset ( $_GET ['id'] ) ? intval ( $_GET ['id'] ) : 0;
		$remark = isset ( $_POST ['remark'] ) ? trim ( $_POST ['remark'] ) : 0;
		$ret = ['status'=>0,'data'=>[],'message'=>''];
		
		$model = new ActivityUser();
		$model->updateActivityUser($id, ['user_remark'=>$remark]);
		echo json_encode($ret);
		die;
	}
}

