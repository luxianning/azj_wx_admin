<?php
define('DIR_ROOT', __DIR__);

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);

// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);
// change the following paths if necessary
$config = DIR_ROOT.'/src/config/main.php';
// ini_set('display_errors', 'on');
// error_reporting(E_ALL);
include_once(DIR_ROOT.'/src/uc_client/config.inc.php');
include_once(DIR_ROOT.'/src/uc_client/client.php');
include_once(DIR_ROOT.'/framework/yii.php');

Yii::createWebApplication($config)->run();
 
