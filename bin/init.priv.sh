chown -R www-data:www-data uc/data
chown -R www-data:www-data mis/src/uc_client/data
chown -R www-data:www-data mis/src/runtime
chown -R www-data:www-data mis/assets
chown -R www-data:www-data mis/upload
chown -R www-data:www-data mis/js/ue_min/php/upload

chown -R www-data:www-data w/data
chown -R www-data:www-data w/uc_client/data

mkdir -p ../wx-app/log ../wx-app/html/cache ../wx-app/html/c
chown -R www-data:www-data ../wx-app/html/cache
chown -R www-data:www-data ../wx-app/html/c
chown -R www-data:www-data ../wx-app/log
